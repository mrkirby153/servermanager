package me.mrkirby153.servermanager.task;

import net.minecraft.server.MinecraftServer;

import java.util.Random;

public abstract class Task {

    private int runFrequency;
    private String name;

    public Task(String name, int runFrequency) {
        this.runFrequency = runFrequency;
        this.name = name;
    }

    public Task(int runFrequency) {
        this("Task-" + new Random().nextInt(9999), runFrequency);
    }

    public Task(String name) {
        this(name, 1);
    }

    public final String getName() {
        return this.name;
    }

    public void onQueue(MinecraftServer server) {

    }

    public abstract void run(MinecraftServer server);

    public final boolean shouldRun(int tick) {
        return tick % this.runFrequency == 0;
    }
}
