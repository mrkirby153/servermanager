package me.mrkirby153.servermanager.task;

import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.world.WorldProvider;
import net.minecraftforge.common.DimensionManager;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

public class TPSMonitor extends Task {

    private HashMap<Integer, RollingAverage> tickTimes = new HashMap<>();
    private HashMap<Integer, Long> lowTickrates = new HashMap<>();

    public TPSMonitor() {
        super("TPSMonitor", 5);
    }

    private static long mean(long[] values) {
        long sum = 0L;
        for (long v : values) {
            sum += v;
        }
        return sum / values.length;
    }

    @Override
    public void run(MinecraftServer server) {
        for (Integer dimId : DimensionManager.getIDs()) {
            RollingAverage averageTick = tickTimes.get(dimId);
            if (averageTick == null)
                tickTimes.put(dimId, averageTick = new RollingAverage(25));
            averageTick.add(BigDecimal.valueOf(calculateDimensionTPS(server, dimId)));
            if (averageTick.getAverage().doubleValue() >= 19.5) {
                notifyOfFullTPS(dimId);
            }
            if (averageTick.getAverage().doubleValue() <= ServerManager.instance.config.getLowTickrateThreshold()) {
                notifyServerOfLowTPS(dimId, averageTick.getAverage());
            }
        }
    }

    private double calculateDimensionTPS(MinecraftServer server, int dimension) {
        double worldTickTime = calculateWorldTickTime(server, dimension);
        return Math.min(1000.0 / worldTickTime, 20);
    }

    private double calculateWorldTickTime(MinecraftServer server, int dimension) {
        return mean(server.worldTickTimes.get(dimension)) * 1.0E-6D;
    }

    private void notifyOfFullTPS(int dimension) {
        if (!lowTickrates.containsKey(dimension))
            return;
        lowTickrates.remove(dimension);
        WorldProvider provider = DimensionManager.getProvider(dimension);
        if (provider == null)
            return;
        String dimensionName = provider.getDimensionName();
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(String.format(Strings.LOW_TPS_RESOLVED, dimensionName, dimension)));

    }

    private void notifyServerOfLowTPS(int dimension, BigDecimal tickrate) {
        if (lowTickrates.containsKey(dimension) && lowTickrates.get(dimension) >= System.currentTimeMillis())
            return;
        lowTickrates.put(dimension, System.currentTimeMillis() + (ServerManager.instance.config.getLowTickrateWarnSeconds() * 1000));
        WorldProvider provider = DimensionManager.getProvider(dimension);
        if (provider == null)
            return;
        String dimensionName = provider.getDimensionName();
        double tick = tickrate.doubleValue();
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(new ChatComponentText(String.format(Strings.LOW_TPS_WARNING, dimensionName, dimension, tick)));
    }

    private class RollingAverage {
        private final Queue<BigDecimal> window = new LinkedList<>();
        private final int period;
        private BigDecimal sum = BigDecimal.ZERO;

        public RollingAverage(int period) {
            this.period = period;
        }

        public void add(BigDecimal num) {
            sum = sum.add(num);
            window.add(num);
            if (window.size() > period) {
                sum = sum.subtract(window.remove());
            }
        }

        public BigDecimal getAverage() {
            if (window.isEmpty()) return BigDecimal.ZERO;
            BigDecimal divisor = BigDecimal.valueOf(window.size());
            return sum.divide(divisor, 2, RoundingMode.HALF_UP);
        }
    }
}
