package me.mrkirby153.servermanager.task;

import cpw.mods.fml.common.ObfuscationReflectionHelper;
import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.UtilTime;
import me.mrkirby153.servermanager.alert.Alert;
import me.mrkirby153.servermanager.alert.AlertHandler;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.network.ServerStatusResponse;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

public class MOTDTask extends Task {

    private String baseMotd;

    private int activeIndex = 0;
    private long nextAlertTime = -1;
    private Alert chosenAlert;

    public MOTDTask() {
        super("MOTD");
    }

    @Override
    public void onQueue(MinecraftServer server) {
        baseMotd = server.getMOTD();
        selectNextAlert();
    }

    @Override
    public void run(MinecraftServer server) {
        if (!ServerManager.instance.config.shouldModifyMOTD())
            return;
        ServerStatusResponse ssr = ObfuscationReflectionHelper.getPrivateValue(MinecraftServer.class, server, "field_147147_p");
        if (System.currentTimeMillis() > nextAlertTime){
            nextAlertTime = System.currentTimeMillis() + 5000;
            selectNextAlert();
        }
        if (ServerManager.instance.shutdownTask.showInMOTD) {
            String toAppend = "\n" + EnumChatFormatting.RED + "Server shutdown in " + EnumChatFormatting.GOLD
                    + ServerManager.instance.shutdownTask.prettyShutdownTime;

            ssr.func_151315_a(new ChatComponentText(baseMotd + toAppend));
        } else if (chosenAlert != null) {
            // There is an active alert
            String time = UtilTime.format(1, chosenAlert.getEndTime() - System.currentTimeMillis(), UtilTime.TimeUnit.FIT);
            String toAppend = String.format("\n" + Strings.ALERT_MOTD_FORMAT, chosenAlert.getMessage(), chosenAlert.getSeparator(), time);
            ssr.func_151315_a(new ChatComponentText(baseMotd + toAppend));
        } else {
            ssr.func_151315_a(new ChatComponentText(baseMotd));
        }
    }

    private void selectNextAlert() {
        int numberOfAlerts = AlertHandler.getInstance().getActiveAlerts().size();
        if (numberOfAlerts < 1) {
            chosenAlert = null;
            return;
        }
        if (this.activeIndex > numberOfAlerts - 1)
            this.activeIndex = 0;
        this.chosenAlert = AlertHandler.getInstance().getActiveAlerts().get(activeIndex++);
    }
}
