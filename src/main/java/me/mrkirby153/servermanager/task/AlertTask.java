package me.mrkirby153.servermanager.task;

import me.mrkirby153.servermanager.UtilTime;
import me.mrkirby153.servermanager.alert.Alert;
import me.mrkirby153.servermanager.alert.AlertHandler;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;

import java.util.ArrayList;
import java.util.Iterator;

public class AlertTask extends Task {

    private ArrayList<Alert> announced = new ArrayList<>();

    public AlertTask() {
        super("Alert-Handler", 2);
    }

    @Override
    public void run(MinecraftServer server) {
        Iterator<Alert> iterator = AlertHandler.getInstance().getAllAlerts().iterator();

        while (iterator.hasNext()) {
            Alert a = iterator.next();
            if (a.isExpired()) {
                iterator.remove();
                announced.remove(a);
                AlertHandler.getInstance().save();
                continue;
            }
            if (a.getStartTime() < System.currentTimeMillis()) {
                if (!announced.contains(a)) {
                    server.getConfigurationManager().sendChatMsg(new ChatComponentText(String.format(Strings.ALERT_ANNOUNCE_ACTIVE, a.getMessage(),
                            a.getSeparator(), UtilTime.format(1, a.getEndTime() - System.currentTimeMillis(), UtilTime.TimeUnit.FIT))));
                    announced.add(a);
                }
            }
        }
        if (AlertHandler.getInstance().getActiveAlerts().size() == 0)
            announced.clear();
    }
}
