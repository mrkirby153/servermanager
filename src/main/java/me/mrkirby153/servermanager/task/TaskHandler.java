package me.mrkirby153.servermanager.task;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.TickEvent;
import me.mrkirby153.servermanager.ServerManager;
import net.minecraft.server.MinecraftServer;
import org.apache.logging.log4j.Level;

import java.util.ArrayList;
import java.util.List;

public class TaskHandler {

    private int tickCount;
    private MinecraftServer server;
    private boolean debug;
    private boolean running = false;

    private List<Task> taskList = new ArrayList<>();

    public TaskHandler(boolean debug) {
        this.tickCount = 0;
        this.debug = debug;
        this.server = MinecraftServer.getServer();
    }

    public TaskHandler() {
        this(false);
    }

    public void queue(Task task) {
        this.taskList.add(task);
        task.onQueue(this.server);
    }

    @SubscribeEvent
    public void serverTick(TickEvent.ServerTickEvent event) {
        // Don't tick unless the server's starting
        if (!running)
            return;
        if (event.phase == TickEvent.Phase.START) {
            taskList.stream().filter(t -> t.shouldRun(tickCount)).forEach(t -> {
                if (debug)
                    ServerManager.instance.logger.debug("Executing task " + t.getName() + " at tick " + tickCount);
                try {
                    t.run(server);
                } catch (Exception e) {
                    e.printStackTrace();
                    ServerManager.instance.notice(Level.ERROR, "The task %s threw an error", t.getName());
                }
            });
            tickCount++;
        }
    }

    public void start() {
        if (debug)
            ServerManager.instance.logger.debug("Started executing tasks");
        running = true;
    }
}
