package me.mrkirby153.servermanager.task;

import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.UtilTime;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import org.apache.logging.log4j.Level;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ShutdownTask extends Task {

    public long shutdownTime;
    public boolean showInMOTD;
    public String prettyShutdownTime;
    private MinecraftServer server;
    private boolean overrideShutdown = false;
    private boolean lateStart = false;

    public ShutdownTask() {
        super("Shutdown", 20);
    }

    @Override
    public void onQueue(MinecraftServer server) {
        this.server = server;
        processShutdownTime();
        if (this.shutdownTime < System.currentTimeMillis() && ServerManager.instance.config.shouldAutoShutdown()) {
            lateStart = true;
            this.prettyShutdownTime = null;
            ServerManager.instance.notice(Level.INFO, "Automatic shutdown disabled, due to starting past the shutdown time (%s)", new SimpleDateFormat(UtilTime.DATE_FORMAT_NOW).format(this.shutdownTime));
        }
        if (!ServerManager.instance.config.shouldAutoShutdown()) {
            this.prettyShutdownTime = null;
            this.showInMOTD = false;
        }
    }

    public void overrideShutdown(long newTime) {
        overrideShutdown = true;
        shutdownTime = newTime;
        processShutdownTime();
        announce();
    }

    @Override
    @SuppressWarnings("unchecked")
    public void run(MinecraftServer server) {
        if (ServerManager.instance.config.shouldAutoShutdown() || overrideShutdown) {
            try {
                long timeUntilShutdown = processShutdownTime();

                if (timeUntilShutdown <= 0 && (!lateStart || overrideShutdown)) {
                    if(server.getConfigurationManager() == null || server.getConfigurationManager().playerEntityList == null)
                        return;
                    server.getConfigurationManager().playerEntityList.stream().filter(o -> o instanceof EntityPlayerMP).forEach(o -> {
                        if (!overrideShutdown)
                            ((EntityPlayerMP) o).playerNetServerHandler.kickPlayerFromServer(ServerManager.instance.config.getKickMessage());
                        else
                            ((EntityPlayerMP) o).playerNetServerHandler.kickPlayerFromServer(Strings.KICK_FORCE_SHUTDOWN);
                    });
                    ServerManager.instance.notice(Level.INFO, "Server shutdown initiated!. Scheduled for %s", new SimpleDateFormat(UtilTime.DATE_FORMAT_NOW).format(shutdownTime));
                    server.initiateShutdown();
                }

                warn();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void announce() {
        ChatComponentText cct = new ChatComponentText(String.format(Strings.ALERT_SHUTDOWN_TIME, prettyShutdownTime.replaceAll("\\.\\d", "")));
        server.getConfigurationManager().sendChatMsg(cct);
    }

    private long processShutdownTime() {
        try {
            if (!overrideShutdown) {
                Calendar now = Calendar.getInstance();
                Calendar shutdownTime = Calendar.getInstance();
                shutdownTime.setTime(new SimpleDateFormat("HH:mm").parse(ServerManager.instance.config.getShutdownTime()));
                shutdownTime.set(Calendar.DAY_OF_YEAR, now.get(Calendar.DAY_OF_YEAR));
                shutdownTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
                this.shutdownTime = shutdownTime.getTimeInMillis();
            }

            long timeUntilShutdown = this.shutdownTime - System.currentTimeMillis();
            prettyShutdownTime = UtilTime.format(0, timeUntilShutdown, UtilTime.TimeUnit.FIT);
            return timeUntilShutdown;
        } catch (ParseException e) {
            return Long.MAX_VALUE;
        }
    }

    private void warn() {
        long millis = this.shutdownTime - System.currentTimeMillis();
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        long subSeconds = seconds - (minutes * 60);

        if (minutes < 15) {
            this.showInMOTD = true;
            if (millis > 0 && seconds <= 10) {
                announce();
                return;
            }
            if (minutes % 5 == 0 && subSeconds == 0) {
                announce();
            }
        } else {
            this.showInMOTD = false;
        }
    }
}
