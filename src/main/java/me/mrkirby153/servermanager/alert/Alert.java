package me.mrkirby153.servermanager.alert;

public class Alert {

    private String id;
    private String message;
    private long startTime;
    private long endTime;

    private Separator separator;

    public Alert(String message, long startTime, long endTime, String separator) {
        this.message = message;
        this.startTime = startTime;
        this.endTime = endTime;
        this.separator = Separator.getSeparator(separator);
    }

    public Alert(String message, long endTime, String separator) {
        this.message = message;
        this.startTime = System.currentTimeMillis();
        this.endTime = endTime;
        this.separator = Separator.getSeparator(separator);
    }

    public long getEndTime() {
        return endTime;
    }

    public String getId() {
        return id;
    }

    protected void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public Separator getSeparator() {
        return separator;
    }

    public long getStartTime() {
        return startTime;
    }

    public boolean isExpired() {
        return this.endTime <= System.currentTimeMillis();
    }

    public enum Separator {
        IN("in"),
        FOR("for");

        private String separator;

        Separator(String text) {
            this.separator = text;
        }

        public static Separator getSeparator(String text) {
            for (Separator s : Separator.values()) {
                if (s.toString().equalsIgnoreCase(text))
                    return s;
            }
            return IN;
        }

        @Override
        public String toString() {
            return separator;
        }
    }
}
