package me.mrkirby153.servermanager.alert;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import cpw.mods.fml.common.gameevent.PlayerEvent;
import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.UtilTime;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;
import net.minecraftforge.event.world.WorldEvent;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class AlertHandler {

    private static final String ID_VALID_CHARS = "abcdefghijklmnopqrstuvwxyz";
    private static final Random random = new Random();
    private static final File saveLocation = new File("alerts.json");
    private static AlertHandler instance;
    private ArrayList<Alert> alerts = new ArrayList<>();
    private ExecutorService executor;

    private AlertHandler() {
        executor = Executors.newCachedThreadPool();
    }

    private static String generateId() {
        String id = "";
        for (int i = 0; i < 5; i++) {
            id += ID_VALID_CHARS.charAt(random.nextInt(ID_VALID_CHARS.length()));
        }
        return id;
    }

    public static AlertHandler getInstance() {
        if (instance == null)
            instance = new AlertHandler();
        return instance;
    }

    public void flushAlerts() {
        Iterator<Alert> iterator = alerts.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().isExpired())
                iterator.remove();
        }
    }

    public List<Alert> getActiveAlerts() {
        flushAlerts();
        return this.alerts.stream().filter(a -> a.getStartTime() < System.currentTimeMillis())
                .filter(a -> a.getEndTime() > System.currentTimeMillis()).collect(Collectors.toList());
    }

    public List<Alert> getAllAlerts() {
        return alerts;
    }

    public void load() {
        try {
            if (!saveLocation.exists())
                save();
            FileReader reader = new FileReader(saveLocation);
            alerts = new Gson().fromJson(reader, new TypeToken<ArrayList<Alert>>() {
            }.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SubscribeEvent
    public void onJoin(PlayerEvent.PlayerLoggedInEvent event) {
        if (ServerManager.instance.shutdownTask.showInMOTD) {
            executor.execute(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // Ignore
                }
                event.player.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + "" + EnumChatFormatting.BOLD + "Server shutdown in " +
                        EnumChatFormatting.GOLD + EnumChatFormatting.BOLD + ServerManager.instance.shutdownTask.prettyShutdownTime));
            });
        } else if (AlertHandler.getInstance().getActiveAlerts().size() > 0) {
            executor.execute(() -> {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                event.player.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));
                for (Alert a : new ArrayList<>(AlertHandler.getInstance().getActiveAlerts())) {
                    event.player.addChatMessage(new ChatComponentText(String.format(Strings.ALERT_FORMAT, a.getMessage(),
                            a.getSeparator(), UtilTime.format(0, a.getEndTime() - System.currentTimeMillis(), UtilTime.TimeUnit.FIT))));
                }
                event.player.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));
            });
        }
    }

    public void queueAlert(Alert alert) {
        alert.setId(generateId());
        alerts.add(alert);
        save();
    }

    public void removeAlert(String id) {
        Iterator<Alert> iterator = alerts.iterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId().equalsIgnoreCase(id))
                iterator.remove();
        }
        save();
    }

    public void save() {
        try {
            String json = new Gson().toJson(alerts);
            if (!saveLocation.exists())
                saveLocation.createNewFile();
            FileWriter writer = new FileWriter(saveLocation);
            writer.write(json);
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SubscribeEvent
    public void worldSave(WorldEvent.Save event) {
        save();
    }
}
