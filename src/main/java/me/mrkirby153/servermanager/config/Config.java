package me.mrkirby153.servermanager.config;

import me.mrkirby153.servermanager.reference.Strings;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;

import java.io.File;

public class Config {

    private static final String MOTD_CATEGORY = "motd";
    private static final String AUTO_STOP_CATEGORY = "shutdown";
    private static final String TICKRATE_CATEGORY = "tickrate";

    private Configuration configuration;

    private boolean modifyMOTD = false;
    private boolean autoShutdown = false;
    private String shutdownTime = "23:00";
    private String kickMessage = "We will return tomorrow";

    private double lowTickrateThreshold = 10.0;
    private boolean monitorTickrate = true;

    private int lowTickrateWarnSeconds = 5;

    public Config(File configFile) {
        configuration = new Configuration(configFile);
        refresh();
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public String getKickMessage() {
        return kickMessage;
    }

    public double getLowTickrateThreshold() {
        return lowTickrateThreshold;
    }

    public String getShutdownTime() {
        return shutdownTime;
    }

    public void refresh() {
        configuration.setCategoryComment(MOTD_CATEGORY, Strings.CFG_CATEGORY_MOTD);
        configuration.setCategoryComment("shutdown", Strings.CFG_CATEGORY_AUTO_SHUTDOWN);

        Property modifyMOTD = configuration.get(MOTD_CATEGORY, "modifyMotd", this.modifyMOTD);
        modifyMOTD.comment = Strings.CFG_MODIFY_MOTD_COMMENT;
        this.modifyMOTD = modifyMOTD.getBoolean();

        Property autoShutdown = configuration.get(AUTO_STOP_CATEGORY, "enable", this.autoShutdown);
        autoShutdown.comment = Strings.CFG_AUTO_SHUTDOWN_COMMENT;
        this.autoShutdown = autoShutdown.getBoolean();

        Property shutdownTime = configuration.get(AUTO_STOP_CATEGORY, "time", this.shutdownTime);
        shutdownTime.comment = Strings.CFG_AUTO_SHUTDOWN_TIME_COMMENT;
        this.shutdownTime = shutdownTime.getString();

        Property kickMessage = configuration.get(AUTO_STOP_CATEGORY, "message", this.kickMessage);
        kickMessage.comment = Strings.CFG_AUTO_SHUTDOWN_KICK_MSG_COMMENT;
        this.kickMessage = kickMessage.getString();

        Property monitorTickrate = configuration.get(TICKRATE_CATEGORY, "monitor_tickrate", this.monitorTickrate);
        monitorTickrate.comment = "Set to false to disable tickrate monitoring";
        this.monitorTickrate = monitorTickrate.getBoolean();

        Property lowTickrate = configuration.get(TICKRATE_CATEGORY, "low_tickrate_threshold", this.lowTickrateThreshold);
        lowTickrate.comment = "The threshold (In TPS) in which to alert the server of a low TPS";
        this.lowTickrateThreshold = lowTickrate.getDouble();

        Property lowTickrateAlertSeconds = configuration.get(TICKRATE_CATEGORY, "low_tickrate_alert", this.lowTickrateWarnSeconds);
        lowTickrate.comment = "The amount of time (in seconds) between low tickrate alerts";
        this.lowTickrateWarnSeconds = lowTickrateAlertSeconds.getInt();


        if (configuration.hasChanged())
            configuration.save();
    }

    public boolean shouldAutoShutdown() {
        return autoShutdown;
    }

    public boolean shouldModifyMOTD() {
        return modifyMOTD;
    }

    public int getLowTickrateWarnSeconds() {
        return lowTickrateWarnSeconds;
    }
}
