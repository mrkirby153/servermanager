package me.mrkirby153.servermanager.reference;

import net.minecraft.util.EnumChatFormatting;

public class Strings {

    public static final String MODID = "ServerManager";
    public static final String VERSION = "@VERSION@";
    public static final String MODNAME = "Server Manager";
    public static final String LOGGER_NAME = "ServerManager";


    // Config
    public static final String CFG_MODIFY_MOTD_COMMENT = "Set to true to enable ServerManager control over the MOTD";
    public static final String CFG_AUTO_SHUTDOWN_COMMENT = "Set to true to have ServerManager automatically shut the server down";
    public static final String CFG_AUTO_SHUTDOWN_TIME_COMMENT = "The time (in 24 hour format) at which the server will be shut down";
    public static final String CFG_CATEGORY_MOTD = "Settings involving changing the MOTD";
    public static final String CFG_CATEGORY_AUTO_SHUTDOWN = "Settings involving the automatic shutdown of the server";
    public static final String CFG_AUTO_SHUTDOWN_KICK_MSG_COMMENT = "The message displayed to the users when they are disconnected";


    public static final String KICK_FORCE_SHUTDOWN = "We will return shortly.";
    public static final String ALERT_SHUTDOWN_TIME = EnumChatFormatting.RED + "Server shutdown in " + EnumChatFormatting.GOLD + "%s";
    public static final String ALERT_SHUTDOWN_ABORTED = EnumChatFormatting.GREEN + "Force shutdown canceled!";

    public static final String CMD_SHUTDOWNQUERY = EnumChatFormatting.GREEN + "Shutdown in " + EnumChatFormatting.GOLD + "%s";

    public static final String CMD_ALERT_GENERAL_USAGE = "/alert <add|remove|list>";
    public static final String CMD_ALERT_INVALID_DATE = "Could not parse the given date: %s";
    public static final String CMD_ALERT_ADDED = EnumChatFormatting.GREEN + "Added alert!";
    public static final String CMD_ALERT_REMOVED = EnumChatFormatting.GREEN + "Removed alert!";

    public static final String CMD_NO_PERMISSION = EnumChatFormatting.DARK_RED + "You do not have permission to perform this command";
    public static final String CMD_NO_PLAYER_FOUND = EnumChatFormatting.RED + "No player was found with that name";

    public static final String CMD_BLOCK_NO_COMMANDS = EnumChatFormatting.RED + "No commands are blocked for this player";

    public static final String ALERT_MOTD_FORMAT = EnumChatFormatting.GREEN + "%s %s " + EnumChatFormatting.GOLD + "%s";
    public static final String ALERT_HEADER = EnumChatFormatting.GOLD + "---------------";
    public static final String ALERT_FORMAT = "   - " + EnumChatFormatting.AQUA + "%s " + EnumChatFormatting.BLUE + "%s " + EnumChatFormatting.GOLD + "%s";
    public static final String ALERT_QUEUED = EnumChatFormatting.GRAY + "QUEUED (%s)";
    public static final String ALERT_NO_ALERTS = EnumChatFormatting.DARK_RED + "No alerts active at this time.";
    public static final String ALERT_ANNOUNCE_ACTIVE = EnumChatFormatting.GOLD + "%s %s " + EnumChatFormatting.RED + "%s";

    public static final String PUNISH_BLOCKED_COMMAND = EnumChatFormatting.ITALIC + "" + EnumChatFormatting.GRAY + "[%s: Attempted to use denied command %s]";
    public static final String NO_SHUTDOWN_SCHEDULED = EnumChatFormatting.GREEN + "There is no shutdown scheduled!";

    public static final String LOW_TPS_WARNING = EnumChatFormatting.YELLOW + "" + EnumChatFormatting.BOLD + "WARNING: " + EnumChatFormatting.WHITE + "Dimension %s (%d) is experiencing" +
            " a low tickrate (%.2f TPS)";
    public static final String LOW_TPS_RESOLVED = EnumChatFormatting.GREEN+"Dimension %s (%d) has returned to 20 TPS";
}
