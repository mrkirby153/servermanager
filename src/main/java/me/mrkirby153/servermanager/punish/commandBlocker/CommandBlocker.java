package me.mrkirby153.servermanager.punish.commandBlocker;

import com.google.gson.Gson;
import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraftforge.event.CommandEvent;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class CommandBlocker {

    private static CommandBlocker instance;
    private final transient File saveLocation = new File("blocked-commands.json");
    private HashMap<UUID, HashSet<String>> blockedCommands = new HashMap<>();
    private HashMap<UUID, HashSet<String>> globalOverrides = new HashMap<>();
    private HashSet<String> globalBlocks = new HashSet<>();


    private CommandBlocker() {
        if (!saveLocation.exists()) {
            save();
        }
    }

    public static CommandBlocker getInstance() {
        if (instance == null)
            instance = new CommandBlocker();
        return instance;
    }

    public void blockCommand(UUID player, String command) {
        if (globalBlocks.contains(command.toLowerCase())) {
            HashSet<String> overrides = globalOverrides.remove(player);
            if (overrides == null)
                overrides = new HashSet<>();
            overrides.remove(command.toLowerCase());
            globalOverrides.put(player, overrides);
            save();
            return;
        } else {
            HashSet<String> blockedCommands = this.blockedCommands.remove(player);
            if (blockedCommands == null)
                blockedCommands = new HashSet<>();

            blockedCommands.add(command.toLowerCase());
            this.blockedCommands.put(player, blockedCommands);
        }
        save();
    }

    public void blockGlobal(String command) {
        this.globalBlocks.add(command.toLowerCase());
        save();
    }

    @SubscribeEvent
    @SuppressWarnings("unchecked")
    public void commandBlocker(CommandEvent event) {
        ICommand command = event.command;
        ICommandSender sender = event.sender;
        // Only block for players. Console can do anything they want
        if (sender instanceof EntityPlayerMP) {
            EntityPlayerMP player = (EntityPlayerMP) sender;
            HashSet<String> blockedCommands = getBlockedCommands(player.getUniqueID());

            if (blockedCommands == null || blockedCommands.size() == 0)
                return;

            if (blockedCommands.contains(command.getCommandName().toLowerCase())) {
                event.setCanceled(true);
                event.exception = new CommandException("You cannot use this command at this time");

                MinecraftServer.getServer().getConfigurationManager().playerEntityList.stream().filter(o -> o instanceof EntityPlayerMP).forEach(o -> {
                    EntityPlayerMP eP = (EntityPlayerMP) o;
                    if (MinecraftServer.getServer().getConfigurationManager().func_152596_g(eP.getGameProfile())) {
                        eP.addChatMessage(new ChatComponentText(String.format(Strings.PUNISH_BLOCKED_COMMAND, player.getCommandSenderName(), command.getCommandName())));
                    }
                });
            }
        }
    }

    public HashSet<String> getBlockedCommands(UUID player) {
        HashSet<String> blocked = this.blockedCommands.get(player);
        if (blocked == null)
            blocked = new HashSet<>();
        blocked.addAll(globalBlocks);

        HashSet<String> overrides = this.globalOverrides.get(player);
        if (overrides != null) {
            blocked.removeAll(overrides);
        }
        return blocked;
    }

    public HashSet<String> getGlobalBlocked() {
        return globalBlocks;
    }

    public void load() {
        try {
            FileReader reader = new FileReader(this.saveLocation);
            CommandBlocker saved = new Gson().fromJson(reader, CommandBlocker.class);
            this.globalOverrides = saved.globalOverrides;
            this.globalBlocks = saved.globalBlocks;
            this.blockedCommands = saved.blockedCommands;
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            String json = new Gson().toJson(this);
            FileWriter writer = new FileWriter(this.saveLocation);
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unblockCommand(UUID player, String command) {
        if (this.globalBlocks.contains(command.toLowerCase())) {
            HashSet<String> globals = this.globalOverrides.remove(player);
            if (globals == null) {
                globals = new HashSet<>();
            }
            globals.add(command.toLowerCase());
            this.globalOverrides.put(player, globals);
        } else {
            HashSet<String> blockedCommands = this.blockedCommands.remove(player);
            if (blockedCommands == null)
                blockedCommands = new HashSet<>();
            blockedCommands.remove(command);
            if (blockedCommands.size() < 1) {
                save();
                return;
            }
            this.blockedCommands.put(player, blockedCommands);
        }
        save();
    }

    public void unblockGlobal(String command) {
        this.globalBlocks.remove(command.toLowerCase());
        Iterator<Map.Entry<UUID, HashSet<String>>> iterator = this.globalOverrides.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<UUID, HashSet<String>> next = iterator.next();
            HashSet<String> blocks = next.getValue();
            blocks.remove(command.toLowerCase());
            if(blocks.size() < 1)
                iterator.remove();
        }
        save();
    }
}
