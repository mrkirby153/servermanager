package me.mrkirby153.servermanager;


import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.*;
import cpw.mods.fml.relauncher.Side;
import me.mrkirby153.servermanager.alert.AlertHandler;
import me.mrkirby153.servermanager.commands.AlertCommand;
import me.mrkirby153.servermanager.commands.CommandBlockCommand;
import me.mrkirby153.servermanager.commands.ScheduleShutdown;
import me.mrkirby153.servermanager.commands.ShutdownQuery;
import me.mrkirby153.servermanager.config.Config;
import me.mrkirby153.servermanager.punish.commandBlocker.CommandBlocker;
import me.mrkirby153.servermanager.reference.Strings;
import me.mrkirby153.servermanager.task.*;
import net.minecraftforge.common.MinecraftForge;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(modid = Strings.MODID, name = Strings.MODNAME, version = Strings.VERSION, acceptableRemoteVersions = "*")
public class ServerManager {

    @Mod.Instance(Strings.MODID)
    public static ServerManager instance;

    public Logger logger;

    public Config config;

    public ShutdownTask shutdownTask;
    public TaskHandler taskHandler;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event){
        logger = LogManager.getLogger(Strings.LOGGER_NAME);
        config = new Config(event.getSuggestedConfigurationFile());
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event){
        if(event.getSide() == Side.SERVER){
            taskHandler = new TaskHandler();
            taskHandler.queue(shutdownTask = new ShutdownTask());
            taskHandler.queue(new AlertTask());
            taskHandler.queue(new MOTDTask());
            taskHandler.queue(new TPSMonitor());

            MinecraftForge.EVENT_BUS.register(AlertHandler.getInstance());
            MinecraftForge.EVENT_BUS.register(CommandBlocker.getInstance());
            FMLCommonHandler.instance().bus().register(taskHandler);
            FMLCommonHandler.instance().bus().register(AlertHandler.getInstance());
        }
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event){

    }

    @Mod.EventHandler
    public void setverStart(FMLServerStartingEvent event){
        if(event.getSide() != Side.SERVER)
            return;

        event.registerServerCommand(new ScheduleShutdown());
        event.registerServerCommand(new ShutdownQuery());
        event.registerServerCommand(new AlertCommand());
        event.registerServerCommand(new CommandBlockCommand());
        AlertHandler.getInstance().load();
        CommandBlocker.getInstance().load();
    }

    @Mod.EventHandler
    public void serverAboutToStart(FMLServerAboutToStartEvent event){
        this.taskHandler.start();
    }

    public void notice(Level level, String message, Object...data){
        logger.log(level, "****************************************");
        logger.log(level, "");
        logger.log(level, "");
        logger.log(level, String.format(message, data));
        logger.log(level, "");
        logger.log(level, "");
        logger.log(level, "****************************************");
    }
}
