package me.mrkirby153.servermanager.commands;

import com.mojang.authlib.GameProfile;
import me.mrkirby153.servermanager.punish.commandBlocker.CommandBlocker;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class CommandBlockCommand extends CommandBase {
    @Override
    @SuppressWarnings("unchecked")
    public List addTabCompletionOptions(ICommandSender sender, String[] args) {
        if (args.length == 1) {
            return getListOfStringsMatchingLastWord(args, "block", "unblock", "list");
        } else {
            if (args.length == 2) {
                String[] usernames = new String[MinecraftServer.getServer().getAllUsernames().length + 1];
                for (int i = 0; i < MinecraftServer.getServer().getAllUsernames().length; i++) {
                    usernames[i] = MinecraftServer.getServer().getAllUsernames()[i];
                }
                usernames[MinecraftServer.getServer().getAllUsernames().length] = "-g";
                return getListOfStringsMatchingLastWord(args, usernames);
            }
            if (args.length == 3) {
                if(args[0].equalsIgnoreCase("block")) {
                    List list = MinecraftServer.getServer().getCommandManager().getPossibleCommands(sender);
                    String[] commands = new String[list.size()];
                    int index = 0;
                    for (Object o : list) {
                        if (o instanceof ICommand) {
                            commands[index++] = ((ICommand) o).getCommandName();
                        }
                    }
                    return getListOfStringsMatchingLastWord(args, commands);
                }
                if(args[0].equalsIgnoreCase("unblock")){
                    if(args[1].equalsIgnoreCase("-g")){
                        return getListOfStringsMatchingLastWord(args, CommandBlocker.getInstance().getGlobalBlocked().toArray(new String[0]));
                    } else {
                        if(Arrays.asList(MinecraftServer.getServer().getAllUsernames()).contains(args[1])){
                            UUID u = getGameProfile(args[1]).getId();
                            return getListOfStringsMatchingLastWord(args, CommandBlocker.getInstance().getBlockedCommands(u).toArray(new String[0]));
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getCommandName() {
        return "blockcommand";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/blockcommand <block|unblock|list> [-g] <name> <command>";
    }

    @Override
    // TODO: 8/4/2016 Make this flow a bit better. Probably a re-write
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("list")) {
                boolean global = false;
                if(args[1].equalsIgnoreCase("-g")){
                    global = true;
                }

                GameProfile profile = !global? getGameProfile(args[1]) : null;
                HashSet<String> blockedCommands = global? CommandBlocker.getInstance().getGlobalBlocked() : CommandBlocker.getInstance().getBlockedCommands(profile.getId());

                sender.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));
                if (blockedCommands.size() < 1) {
                    sender.addChatMessage(new ChatComponentText(Strings.CMD_BLOCK_NO_COMMANDS));
                } else {
                    for (String s : blockedCommands) {
                        sender.addChatMessage(new ChatComponentText(s));
                    }
                }
                sender.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));
                return;
            }
        }
        if (args.length >= 3) {
            String action = args[0];
            boolean global = args[1].equalsIgnoreCase("-g");
            String name = global ? args[2] : args[1];
            String command = global ? args[2] : args[2];

            GameProfile profile = global? null : getGameProfile(name);

            if (action.equalsIgnoreCase("block")) {
                // Prevent the user from accidentally denying themselves access to this command
                if (command.equalsIgnoreCase(this.getCommandName())) {
                    throw new CommandException("You cannot block this command");
                }
                if (global) {
                    CommandBlocker.getInstance().blockGlobal(command);
                    CommandBase.func_152373_a(sender, this, "Blocked command %s globally", command);
                } else {
                    CommandBlocker.getInstance().blockCommand(profile.getId(), command);
                    CommandBase.func_152373_a(sender, this, "Blocked command %s for %s", command, profile.getName());
                }
                return;
            }

            if (action.equalsIgnoreCase("unblock")) {
                if (global) {
                    CommandBlocker.getInstance().unblockGlobal(command);
                    CommandBase.func_152373_a(sender, this, "Unblocked command %s globally", command);
                } else {
                    CommandBlocker.getInstance().unblockCommand(profile.getId(), command);
                    CommandBase.func_152373_a(sender, this, "Unblocked command %s for %s", command, profile.getName());
                }
                return;
            }
        }
        throw new CommandException(EnumChatFormatting.RED + "Usage: " + getCommandUsage(sender));

    }

    private GameProfile getGameProfile(String name) {
        GameProfile profile = MinecraftServer.getServer().func_152358_ax().func_152655_a(name); // MinecraftServer.getServer().getPlayerProfileCache().getGameProfileForUsername()
        if (profile == null) {
            throw new CommandException(Strings.CMD_NO_PLAYER_FOUND);
        }
        return profile;
    }
}
