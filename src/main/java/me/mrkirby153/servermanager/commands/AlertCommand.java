package me.mrkirby153.servermanager.commands;

import me.mrkirby153.servermanager.UtilTime;
import me.mrkirby153.servermanager.alert.Alert;
import me.mrkirby153.servermanager.alert.AlertHandler;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.regex.Pattern;

public class AlertCommand extends CommandBase {

    private static final String DATE_FORMAT_REGEX = "(\\d{1,2}-?){2}\\d{4}\\s(\\d{1,2}:?){3}";

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }

    @Override
    public String getCommandName() {
        return "alert";
    }

    @Override
    public String getCommandUsage(ICommandSender sender) {
        return Strings.CMD_ALERT_GENERAL_USAGE;
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            showAlerts(sender);
        }
        if (args.length == 1) {
            if (args[0].equalsIgnoreCase("help")) {
                throw new CommandException("Usage: " + getCommandUsage(sender));
            }
            if (args[0].equalsIgnoreCase("list")) {
                showAlerts(sender);
                return;
            }
        }
        if (args.length == 2) {
            if (args[0].equalsIgnoreCase("remove")) {
                String id = args[1];
                AlertHandler.getInstance().removeAlert(id);
                sender.addChatMessage(new ChatComponentText(Strings.CMD_ALERT_REMOVED));
                return;
            }
        }
        if (args.length >= 4) {
            if (args[0].equalsIgnoreCase("add")) {
                if (!isOp(sender))
                    throw new CommandException(Strings.CMD_NO_PERMISSION);

                String separator = args[1];
                boolean hasEndTime = args.length >= 6;
                String startDate = args[2] + " " + args[3];
                String endDate = null;

                // Verify that the end time actually exists
                if (hasEndTime) {
                    endDate = args[4] + " " + args[5];
                    hasEndTime = Pattern.compile(DATE_FORMAT_REGEX).matcher(endDate).find();
                }

                String message = "";
                for (int i = hasEndTime ? 6 : 4; i < args.length; i++) {
                    message += args[i] + " ";
                }
                message = message.trim();

                long startTime, endTime;
                if (hasEndTime) {
                    startTime = parseDate(startDate);
                    endTime = parseDate(endDate);
                } else {
                    startTime = System.currentTimeMillis();
                    endTime = parseDate(startDate);
                }

                Alert newAlert = new Alert(message, startTime, endTime, separator);
                AlertHandler.getInstance().queueAlert(newAlert);
                sender.addChatMessage(new ChatComponentText(Strings.CMD_ALERT_ADDED));
            }
        }
    }

    private boolean isOp(ICommandSender sender) {
        return MinecraftServer.getServer().getConfigurationManager().func_152603_m().func_152700_a(sender.getCommandSenderName()) != null;
    }

    private long parseDate(String date) {
        long time;
        try {
            SimpleDateFormat format = new SimpleDateFormat(UtilTime.DATE_FORMAT_NOW);
            Date parse = format.parse(date);
            time = parse.getTime();
            return time;
        } catch (ParseException e) {
            throw new CommandException(String.format(Strings.CMD_ALERT_INVALID_DATE, date));
        }
    }

    private void showAlerts(ICommandSender sender) {
        sender.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));

        if (AlertHandler.getInstance().getAllAlerts().size() != 0) {
            if (AlertHandler.getInstance().getActiveAlerts().size() == 0 && !isOp(sender)) {
                sender.addChatMessage(new ChatComponentText(Strings.ALERT_NO_ALERTS));
            } else {

                ArrayList<Alert> alerts = new ArrayList<>(AlertHandler.getInstance().getAllAlerts());
                ArrayList<Alert> queuedAlerts = new ArrayList<>();

                Iterator<Alert> it = alerts.iterator();
                while (it.hasNext()) {
                    Alert next = it.next();
                    if (next.getStartTime() > System.currentTimeMillis()) {
                        queuedAlerts.add(next);
                        it.remove();
                    }
                }

                alerts.sort((a1, a2) -> (int) (a2.getStartTime() - a1.getStartTime()));

                for (Alert a : alerts) {
                    sender.addChatMessage(new ChatComponentText(String.format(Strings.ALERT_FORMAT, a.getMessage(), a.getSeparator(), UtilTime.format(0, a.getEndTime() - System.currentTimeMillis(), UtilTime.TimeUnit.FIT))));
                    if (isOp(sender))
                        sender.addChatMessage(new ChatComponentText("      " + EnumChatFormatting.DARK_GREEN + a.getId()));
                }

                if (isOp(sender))
                    for (Alert a : queuedAlerts) {
                        String queuedDate = " " + String.format(Strings.ALERT_QUEUED, new SimpleDateFormat("MMM dd YYY HH:mm").format(a.getStartTime()));
                        sender.addChatMessage(new ChatComponentText(String.format(Strings.ALERT_FORMAT + queuedDate, a.getMessage(), a.getSeparator(), UtilTime.format(0, a.getEndTime() - a.getStartTime(), UtilTime.TimeUnit.FIT))));
                        sender.addChatMessage(new ChatComponentText("      " + EnumChatFormatting.DARK_GREEN + a.getId()));
                    }
            }
        } else {
            sender.addChatMessage(new ChatComponentText(Strings.ALERT_NO_ALERTS));
        }

        sender.addChatMessage(new ChatComponentText(Strings.ALERT_HEADER));
    }

}
