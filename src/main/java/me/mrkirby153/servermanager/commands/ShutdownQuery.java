package me.mrkirby153.servermanager.commands;

import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.reference.Strings;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;

public class ShutdownQuery extends CommandBase{
    @Override
    public String getCommandName() {
        return "shutdownquery";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/shutdownquery";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if(ServerManager.instance.shutdownTask.prettyShutdownTime != null)
        sender.addChatMessage(new ChatComponentText(String.format(Strings.CMD_SHUTDOWNQUERY, ServerManager.instance.shutdownTask.prettyShutdownTime)));
        else
            sender.addChatMessage(new ChatComponentText(Strings.NO_SHUTDOWN_SCHEDULED));
    }

    @Override
    public boolean canCommandSenderUseCommand(ICommandSender p_71519_1_) {
        return true;
    }
}
