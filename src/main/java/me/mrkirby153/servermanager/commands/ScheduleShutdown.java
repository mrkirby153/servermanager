package me.mrkirby153.servermanager.commands;

import me.mrkirby153.servermanager.ServerManager;
import me.mrkirby153.servermanager.UtilTime;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.EnumChatFormatting;

import java.util.HashMap;

public class ScheduleShutdown extends CommandBase {
    private static HashMap<String, Integer> time = new HashMap<>();

    static {
        time.put("s", 1);
        time.put("m", 60);
        time.put("h", 3600);
    }

    @Override
    public String getCommandName() {
        return "scheduleshutdown";
    }

    @Override
    public String getCommandUsage(ICommandSender p_71518_1_) {
        return "/scheduleshutdown [XXhXXmXXs]";
    }

    @Override
    public void processCommand(ICommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.addChatMessage(new ChatComponentText(EnumChatFormatting.RED + getCommandUsage(sender)));
            return;
        }
        if (args.length == 1) {
            ServerManager.instance.shutdownTask.overrideShutdown(UtilTime.time(args[0]));
        }
    }
}
